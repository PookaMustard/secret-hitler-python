from random import *
import os
import json

# Defining the save file
SAVE_FILE = "SecretHitlerData.json"

# Defining the investigative powers used in the game as a dict
POWERS = {
    5: {
        1: "NULL",
        2: "NULL",
        3: "POLICY_PEAK",
        4: "EXECUTION",
        5: "EXECUTION",
        6: "NULL"
    },
    7: {
        1: "NULL",
        2: "INVESTIGATE",
        3: "SPECIAL_ELECTION",
        4: "EXECUTION",
        5: "EXECUTION",
        6: "NULL"
    },
    9: {
        1: "INVESTIGATE",
        2: "INVESTIGATE",
        3: "SPECIAL_ELECTION",
        4: "EXECUTION",
        5: "EXECUTION",
        6: "NULL"
    }
}

# Defining the number of fascists depending on number of players as a dict
FASCIST_COUNT = {
    5: 2,
    6: 2,
    7: 3,
    8: 3,
    9: 4,
    10: 4
}

# The expected number of liberal and fascist policies in play
FASCIST_POLICIES = 11
LIBERAL_POLICIES = 6


def loadData():
    """Load all game data into memory.

    Returns:
    the JSON pertaining to the saved games"""
    with open(SAVE_FILE, 'r') as saveData:
        return json.load(saveData)


def saveData(gameData):
    """Save all game data into memory.

    Keyword arguments:
    gameData -- the variable holding all game data
    """
    with open(SAVE_FILE, 'w') as saveData:
        json.dump(gameData, saveData, indent=4)
    return


def checkIntInput(input, downBound, upBound, messageSet):
    """Check if an input follows a set of constraints, like being an int

    Keyword arguments:
    input -- the string being input
    downBound -- the lower bound of int the function shall accept as input
    upBound -- the upper bound of int the function shall accept as input
    messageSet -- show a a different message set depending on context.
        used only to differentiate between regular int checks and
        player participation checks

    Returns:
    string -- The input check message. "OK" is success, else it is failure
    int -- The original user choice. If failure, it becomes 0
    """
    if len(input) == 0:
        return "Please input a number before proceeding", int(0)

    # If the input is indeed an integer, test if it follows the constraints
    # If not, return a generic error message
    try:
        input = int(input)
        if messageSet == "ints":
            if input > upBound or input < downBound:
                return "Please input a valid number " +\
                    "corresponding to a choice.", int(0)
        elif messageSet == "roles":
            if input > upBound:
                return "There can't be more than 10 " + \
                    "players participating.", int(0)
            elif input < downBound:
                return "There can't be less than 5 " + \
                    "players participating.", int(0)
        return "OK", input
    except:
        return "Your input wasn't a number. Please input a number.", int(0)


def openingFrontend():
    """The main menu of the script."""
    os.system('cls||clear')

    # If the save file does not exist, create a new one
    try:
        saveFile = open(SAVE_FILE, 'r')
        saveFile.close()
    except IOError:
        with open(SAVE_FILE, 'w') as saveData:
            init = {
                "game": {}
            }
            json.dump(init, saveData, indent=4)

    # The main loop of the front end
    inputCheck = ""
    while True:
        if not inputCheck:
            print("Welcome to Secret Hitler.\n" +
                  "Coded by Pooka.")
        else:
            print(inputCheck)

        print("\n\nInput the number corresponding to your " +
              "choice, then hit RETURN.\n\n")

        print("1) START NEW GAME\n" +
              "2) LOAD AN EXISTING GAME\n" +
              "3) DELETE AN EXISTING GAME\n" +
              "4) EXIT PROGRAM\n")

        programChoice = input("> ")

        # Verify input follows constraints
        inputCheck, programChoice = checkIntInput(programChoice, 1, 4, "ints")
        if inputCheck != "OK":
            os.system('cls||clear')
            continue
        else:
            if programChoice != 3:
                os.system('cls||clear')
            break

    # User wants to start a new game
    if programChoice == 1:
        startGame()

    # User wants to load a game
    if programChoice == 2:
        loadGameMenu()

    # User wants to delete a game
    if programChoice == 3:
        deleteGame()

    # User wants to exit the program entirely
    if programChoice == 4:
        exit()


def loadGameMenu():
    """The load game menu. Accessed from LOAD AN EXISTING GAME"""
    os.system('cls||clear')
    gameData = loadData()

    # If there is no game data, no point in executing further
    if not gameData['game']:
        print("There are no saved games.")
        input("Press Return to return to main menu.\n")
        openingFrontend()

    print("Currently saved games:\n")

    # Collect saved games from the save data into a dict
    gameNameStore = {}
    gameNum = 0
    for gameName in gameData['game']:
        gameNum += 1
        gameNameStore[gameNum] = gameName
        print(str(gameNum) + ") " + gameName)

    print("\nEnter the number of the game you wish to load.\n" +
          "Enter 0 to cancel.\n")

    # This loop asks for the game you're trying to load, or if you don't
    # want to load a game to begin with, if no game is loaded,
    # you are sent back to the main menu
    inputCheck = ""
    while True:
        loadNumber = input("> ")
        inputCheck, loadNumber = checkIntInput(loadNumber, 0, gameNum, "ints")

        if inputCheck != "OK":
            print("\n" + inputCheck)
            continue
        else:
            if loadNumber != 0:
                loadGameName = gameNameStore[loadNumber]
                input("Press Return to continue.\n")
                loadGame(loadGameName)
            else:
                input("\nLoading cancelled. Press Return to continue.\n")
                openingFrontend()


def deleteGame():
    """The game deletion menu. Accessed from DELETE AN EXISTING GAME"""
    os.system('cls||clear')
    gameData = loadData()

    # If there is no game data, no point in executing further
    if not gameData['game']:
        print("There are no saved games.")
        input("Press Return to return to main menu.\n")
        openingFrontend()

    print("Currently saved games:\n")

    # Collect saved games from the save data into a dict
    gameNameStore = {}
    gameNum = 0
    for gameName in gameData['game']:
        gameNum += 1
        gameNameStore[gameNum] = gameName
        print(str(gameNum) + ") " + gameName)

    print("\nEnter the number of the game you wish to delete.\n" +
          "Enter 0 to cancel.\n")

    # This loop includes several things, including retries for invalid
    # inputs, double checking to make sure the user wants to delete a
    # selected game and cancelling if the user does not want to delete
    # any game, function eventually ends with going back to the main menu
    inputCheck = ""
    while True:
        deleteNumber = input("> ")
        inputCheck, deleteNumber = checkIntInput(
            deleteNumber, 0, gameNum, "ints")

        if inputCheck != "OK":
            print("\n" + inputCheck)
            continue
        else:
            if deleteNumber != 0:
                choice = input("\nAre you sure you want to delete the game " +
                               gameNameStore[deleteNumber] + "? (Y/n)\n> ")
                if choice.lower() == "y":
                    break
                else:
                    input("Deletion cancelled. Press Return to continue.\n")
                    openingFrontend()
            else:
                input("\nDeletion cancelled. Press Return to continue.\n")
                openingFrontend()

    # The code responsible for the actual deletion of a game
    deletedGameName = gameNameStore[deleteNumber]
    del gameData['game'][deletedGameName]
    saveData(gameData)

    print("\n\nSaved game \"" + deletedGameName + "\" successfully deleted.")
    input("Press Return to continue.")
    openingFrontend()


def startGame():
    """The game creation menu. Accessed from START NEW GAME"""
    gameData = loadData()

    # Retrieve game name for save data purposes
    # Game names that are already saved will NOT be saved as duplicates
    gameName = input("Please enter a name for this game:\n> ")
    while True:
        if gameName in gameData["game"]:
            gameName = input("This game name is already saved. " +
                             "Please enter another name:\n> ")
            continue
        else:
            break

    # Retrieve whether the user is a host or a player.
    # Hosting will not make the user a player, but a moderator for the game
    # Playing should be "pass-and-play" style, with the user able to play
    # This is also saved to the save data
    inputCheck = ""
    while True:
        if inputCheck:
            print(inputCheck + "\n\n")

        print("\nAre you hosting or are you playing in the game?\n\n" +
              "1) Playing - Pass and play based gameplay.\n" +
              "2) Hosting - You will see the policy deck and " +
              "player alignments.\n")

        programChoice = input("> ")
        inputCheck, programChoice = checkIntInput(programChoice, 1, 2, "ints")

        if inputCheck != "OK":
            os.system('cls||clear')
            continue
        else:
            isHosting = programChoice - 1
            break

    # Count the number of participating players and ensure constraints
    # This is important for determining the table rules and fascist powers
    inputCheck = ""
    while True:
        if inputCheck:
            print(inputCheck + "\n\n")
        playerCount = input("\nHow many players are participating?\n> ")
        inputCheck, playerCount = checkIntInput(playerCount, 5, 10, "roles")

        if inputCheck != "OK":
            os.system('cls||clear')
            continue
        else:
            os.system('cls||clear')
            break

    # Obtain player names one by one
    # No player cannot have the same name as another player
    currentPlayer = 0
    displayedPlayer = 1
    playerList = []
    while currentPlayer < playerCount:
        playerName = input("Enter Player " + str(displayedPlayer) +
                           "'s name. (" + str(displayedPlayer) + "/" +
                           str(playerCount) + ")\n> ")
        if len(playerName) <= 0:
            print("Invalid name. Try again.\n")
            continue
        if playerName in playerList:
            print("Name already taken. Please enter another name.")
            continue
        playerList.append(playerName)
        currentPlayer += 1
        displayedPlayer += 1

    # Shuffling the player order, can be skipped by typing N
    choice = input("\nThe player order will be shuffled. " +
                   "If you wish not to do that, enter N.\n> ")
    if choice.lower() != "n":
        shuffle(playerList)

    # Duplicating list for the purposes of generating the fascists
    # Duplicated list is "editedPlayerList"
    editedPlayerList = list(playerList)
    fascistCount = FASCIST_COUNT[len(playerList)]

    # Fascists are generated by being popped from the duplicated list
    # Popped fascists go to the new list "fascistList"
    poppedFascistCount = 0
    fascistList = []

    while poppedFascistCount < fascistCount:
        ListLen = len(editedPlayerList) - 1
        fascistList.append(editedPlayerList.pop(randint(0, ListLen)))
        poppedFascistCount += 1

    # Hitler is randomly selected from "fascistList"
    hitlerPlayer = fascistList[randint(0, len(fascistList) - 1)]

    # Collecting the previous information into a dict
    # This dict stores players as numbers
    # Each number has the player's name and alignment
    finalPlayerList = []
    for playerName in playerList:
        if playerName in fascistList:
            if playerName == hitlerPlayer:
                playerAlignment = "Hitler"
            else:
                playerAlignment = "Fascist"
        else:
            playerAlignment = "Liberal"
        currentIter = {
            'playerName': playerName,
            'playerAlignment': playerAlignment,
            'vote': None,
            'isInvestigated': None,
            'isExecuted': None
        }
        finalPlayerList.append(currentIter)

    # Generating the first policy deck and shuffling it
    # The main deck is shuffled, split into two uneven halves
    # the halves are shuffled again before being
    # merged into a main deck once more
    policyDeck = list("F" * FASCIST_POLICIES + "L" * LIBERAL_POLICIES)
    shuffle(policyDeck)
    splitDeck1 = policyDeck[:int(len(policyDeck) / 2)]
    splitDeck2 = policyDeck[int(len(policyDeck) / 2):]
    shuffle(splitDeck1)
    shuffle(splitDeck2)
    policyDeck = splitDeck1 + splitDeck2

    # Depending on whether playing or hosting, the program
    # defines which context it's going to use next.
    if isHosting:
        currentContext = "NOMINATION"
    else:
        currentContext = "ROLE_DISTRIBUTION"

    # Collecting all the previously generated information
    # as well as initializing other relevant information
    # and finally saving it to the save data
    gameData['game'][gameName] = {}
    thisGame = gameData['game'][gameName]

    thisGame['players'] = finalPlayerList
    thisGame['policyDeck'] = policyDeck
    thisGame['isHosting'] = isHosting
    thisGame['currentRound'] = {
        "roundNumber": 1,
        "currentPresident": 0,
        "currentChancellor": None,
        "votes": {},
        "termLocked": [],
        "isPowerActive": "NULL",
        "isVetoRequested": 0,
        "isSpecialElection": 0,
        "governmentVotes": {},
        "policiesInHand": [],
        "policiesDrawn": []
    }
    thisGame['status'] = {
        "liberalEnacted": 0,
        "fascistEnacted": 0,
        "discardPile": 0,
        "currentDeck": 1,
        "nextPresident": 1,
        "governmentTracker": 0,
        "currentContext": currentContext,
        "selectedPlayer": 0,
        "alivePlayers": playerCount
    }
    thisGame['history'] = {}
    historicalDeck = ""
    for policy in policyDeck:
        historicalDeck += policy
    thisGame['history']["deck1"] = historicalDeck
    saveData(gameData)
    loadGame(gameName)


def loadGame(gameName):
    """Properly loads the game JSON file into memory

    Keyword arguments:
    gameName -- the game's name
    """
    gameData = loadData()

    # Defining top level dicts into variables
    gamePlayers = gameData['game'][gameName]['players']
    gameStatus = gameData['game'][gameName]['status']
    currentRound = gameData['game'][gameName]['currentRound']
    policyDeck = gameData['game'][gameName]['policyDeck']
    discardPile = gameData['game'][gameName]['status']['discardPile']
    isHosting = gameData['game'][gameName]['isHosting']
    gameHistory = gameData['game'][gameName]['history']

    # Defining all relevant game information into variables
    currentContext = gameStatus['currentContext']
    currentRoundNumber = currentRound['roundNumber']
    currentDeck = gameStatus['currentDeck']
    isPowerActive = currentRound['isPowerActive']
    isSpecialElection = currentRound['isSpecialElection']
    isVetoRequested = currentRound['isVetoRequested']
    policiesInHand = currentRound['policiesInHand']
    policiesDrawn = currentRound['policiesDrawn']
    alivePlayers = gameStatus['alivePlayers']
    selectedPlayer = gameStatus['selectedPlayer']
    selectedPlayerName = gamePlayers[selectedPlayer]['playerName']
    termLocked = currentRound['termLocked']
    nextPresident = gameStatus['nextPresident']
    currentPresident = currentRound['currentPresident']
    currentPresidentName = gamePlayers[currentPresident]['playerName']
    currentChancellor = currentRound['currentChancellor']
    powerTarget = None
    if currentChancellor is not None:
        currentChancellorName = gamePlayers[currentChancellor]['playerName']

    # Defining card information, especially for later display
    libEnacted = int(gameStatus['liberalEnacted'])
    fasEnacted = int(gameStatus['fascistEnacted'])
    govTracker = int(gameStatus['governmentTracker'])
    libNotEnacted = 5 - libEnacted
    fasNotEnacted = 6 - fasEnacted
    govNotTracker = 3 - govTracker

    # This function shuffles every deck after the first,
    # same as the one in startGame() but it takes into acccount
    # that there are less policies in the policy deck/discard pile,
    # returns the policy deck itself, the discard pile, and
    # the last variable is a boolean reflecting if a new deck
    # was generated
    def shuffleDeck(policyDeck, discardPile):
        """This function shuffles every deck after the first.

        Keyword arguments:
        policyDeck -- the policy deck of the game when function is used
        discardPile -- the discard pile of the game when function is used

        Returns:
        policyDeck -- the result of the policy deck itself
        discardPile -- the result of the discard pile itself
        bool - a boolean reflecting whether a new deck was generated
        """
        if len(policyDeck) < 3:
            # Remaining policies in play are accounted for
            remainingLiberals = LIBERAL_POLICIES - libEnacted
            remainingFascists = FASCIST_POLICIES - fasEnacted
            # New policy deck is generated
            policyDeck = list("F" * remainingFascists +
                              "L" * remainingLiberals)
            # The new policy deck is shuffled, split into two halves,
            # each half is shuffled, then combined again.
            shuffle(policyDeck)
            splitDeck1 = policyDeck[:int(len(policyDeck) / 2)]
            splitDeck2 = policyDeck[int(len(policyDeck) / 2):]
            shuffle(splitDeck1)
            shuffle(splitDeck2)
            policyDeck = splitDeck1 + splitDeck2
            # Empty discard pile
            discardPile = 0
            return policyDeck, discardPile, 1
        else:
            return policyDeck, discardPile, 0

    def saveGame():
        """Saves all relevant game data into the JSON file."""
        gameData['game'][gameName]['players'] = gamePlayers
        gameData['game'][gameName]['policyDeck'] = policyDeck
        gameData['game'][gameName]['history'] = gameHistory
        gameData['game'][gameName]['currentRound'] = currentRound
        gameData['game'][gameName]['currentRound'][
            'currentPresident'] = currentPresident
        gameData['game'][gameName]['currentRound'][
            'currentChancellor'] = currentChancellor
        gameData['game'][gameName]['currentRound'][
            'roundNumber'] = currentRoundNumber
        gameData['game'][gameName]['currentRound'][
            'policiesInHand'] = policiesInHand
        gameData['game'][gameName]['currentRound'][
            'policiesDrawn'] = policiesDrawn
        gameData['game'][gameName]['currentRound'][
            'isSpecialElection'] = isSpecialElection
        gameData['game'][gameName]['currentRound'][
            'termLocked'] = termLocked
        gameData['game'][gameName]['currentRound'][
            'isPowerActive'] = isPowerActive
        gameData['game'][gameName]['currentRound'][
            'isVetoRequested'] = isVetoRequested
        gameData['game'][gameName]['status'] = gameStatus
        gameData['game'][gameName]['status']['governmentTracker'] = govTracker
        gameData['game'][gameName]['status']['discardPile'] = discardPile
        gameData['game'][gameName]['status']['nextPresident'] = nextPresident
        gameData['game'][gameName]['status']['alivePlayers'] = alivePlayers
        gameData['game'][gameName]['status']['liberalEnacted'] = libEnacted
        gameData['game'][gameName]['status']['fascistEnacted'] = fasEnacted
        gameData['game'][gameName]['status']['currentContext'] = currentContext
        gameData['game'][gameName]['status']['selectedPlayer'] = selectedPlayer
        gameData['game'][gameName]['status']['currentDeck'] = currentDeck
        saveData(gameData)
        return

    # This function is used to record every govvernment in the game,
    # along with any actions and enacted policies, there is not yet
    # a function to export said history from JSON
    def recordHistory(roundContext):
        """Records every government along with actions and policies.
        Note that exporting history is not yet supported.

        Keyword arguments
        roundContext -- details
        """
        # If roundContext is NEW_DECK, record info pertaining to the new deck
        if roundContext == "NEW_DECK":
            # Get information for the string this deck will be saved under
            nonlocal currentDeck
            currentDeck += 1
            deckName = "deck" + \
                str(currentDeck)
            # Collecting deck's policy cards
            historicalDeck = ""
            for policy in policyDeck:
                historicalDeck += policy
            # Saving deck to history
            gameHistory[deckName] = historicalDeck
        # If roundContext is not NEW_DECK, record government info
        else:
            # Initializing history for current round
            gameHistory[currentRoundNumber] = {}
            # Saving current context for current round
            if roundContext == "FAILED_GOVERNMENT":
                gameHistory[
                    currentRoundNumber]['status'] = "FAILED_GOVERNMENT"
            elif roundContext == "VETOED_GOVERNMENT":
                gameHistory[
                    currentRoundNumber]['status'] = "VETOED_GOVERNMENT"
            elif roundContext == "LIBERAL_GOVERNMENT":
                gameHistory[
                    currentRoundNumber]['status'] = "LIBERAL_GOVERNMENT"
            elif roundContext == "FASCIST_GOVERNMENT":
                gameHistory[
                    currentRoundNumber]['status'] = "FASCIST_GOVERNMENT"
                # For Fascist governments, also record power used and target
                gameHistory[
                    currentRoundNumber]['isPowerActive'] = isPowerActive
                if isPowerActive != "POLICY_PEAK" or isPowerActive != "NULL":
                    gameHistory[
                        currentRoundNumber]['powerTarget'] = powerTarget
            # Record other variables, including votes
            gameHistory[currentRoundNumber]['enactedPolicy'] = enactedCard
            gameHistory[currentRoundNumber]['policiesDrawn'] = policiesDrawn
            gameHistory[currentRoundNumber]['president'] = currentPresident
            gameHistory[currentRoundNumber]['chancellor'] = currentChancellor
            gameHistory[currentRoundNumber]['votes'] = {}
            for seat, player in enumerate(gamePlayers):
                gameHistory[currentRoundNumber]['votes'][seat] = player['vote']
        return

    def enactTopDeck():
        """Enacts the top card of the policy deck.

        Returns:
        libEnacted -- The number of enacted Liberal policies
        fasEnacted -- The number of enacted Fascist policies
        """
        nonlocal libEnacted
        nonlocal fasEnacted
        # Obtain top of the policy deck, and remove it from the deck
        topDeck = policyDeck.pop(0)
        # Enact policy depending on what the top deck card is
        if topDeck == "L":
            libEnacted += 1
            print("A liberal policy has been enacted!")
        else:
            fasEnacted += 1
            print("A fascist policy has been enacted! No powers will be used.")
        return libEnacted, fasEnacted

    def retrieveFascistPower():
        """Retrieves power associated to number of enacted Fascist policies

        Returns:
        The associated executive power from the POWERS dict
        """
        # Set the appropriate player count, as 5 and 6 player games are
        # similar, and so are 7 and 8 games, and 9 and 10 games
        if len(gamePlayers) == 6 or len(gamePlayers) == 5:
            playerCount = 5
        elif len(gamePlayers) == 7 or len(gamePlayers) == 8:
            playerCount = 7
        else:
            playerCount = 9
        # Returns the power associated with the number of players
        # and Fascist policies enacted
        return POWERS[playerCount][fasEnacted]

    def displayVotes(discloseVotes):
        """Prints player votse on screen.

        Keyword arguments:
        discloseVotes - A bool that determines if players should be
            able to see who voted or what, or not. This is overriden
            if isHosting is true
        """
        # Every player is looped through, including executed ones
        for seat, player in enumerate(gamePlayers):
            # Initializes print message, with player name and seat
            message = str(seat + 1) + " - " + player['playerName'] + " - "
            # If a player is executed, the vote results will reflect such
            if player['isExecuted']:
                message += "Executed"
            else:
                if not player['vote']:
                    message += "No vote yet"
                else:
                    # If the game is being hosted and not "pass and play",
                    # show who voted what at all times
                    if isHosting:
                        if player['vote'] == "y":
                            message += "ja!"
                        elif player['vote'] == "n":
                            message += "nein"
                    # If the game is "pass and play", don't show specific
                    # votes unless discloseVotes is True
                    else:
                        if discloseVotes:
                            if player['vote'] == "y":
                                message += "ja!"
                            elif player['vote'] == "n":
                                message += "nein"
                        else:
                            message += "Voted"
            # Print the resulting message
            print(message)
        return

    def retrieveNextPresident(isSpecialElection, currentPresident):
        """Determines who is the next president.

        Keyword arguments:
        isSpecialElection -- Bool determining if previous gov is special
        currentPresident -- int representing current president's seat number

        Returns:
        currentPresident - int representing new president's seat number
        """
        if isSpecialElection:
            # The new president should be nextPresident, which was defined
            # prior to any successful special elections
            currentPresident = nextPresident
            while True:
                # If nextPresident represents an executed player, skip
                # to the next player in line (note there can't be more than
                # 1 player executed right after a special election)
                if gamePlayers[currentPresident]['isExecuted']:
                    if currentPresident + 1 == len(gamePlayers):
                        currentPresident = 0
                    else:
                        currentPresident += 1
                else:
                    break
        else:
            while True:
                # If the previous government is not special, select next
                # president normally, without selecting executed players
                if currentPresident + 1 == len(gamePlayers):
                    currentPresident = 0
                else:
                    currentPresident += 1
                if gamePlayers[currentPresident]['isExecuted']:
                    continue
                else:
                    break
        return currentPresident

    def verifyChoice(choiceContext):
        """Confirm if player is sure of their choice.

        Keyword arguments:
        choiceContext -- The context of the choice, determines message set
        """
        if choiceContext == "GENERIC":
            verifyChoice = input("\nBefore confirming your choice, " +
                                 "make sure you are the only one " +
                                 "seeing the contents of the screen. " +
                                 "(Y/n)\n> ")
        elif choiceContext == "CONFIRM_INVESTIGATION":
            verifyChoice = input("ARE YOU SURE YOU " +
                                 "WISH TO INVESTIGATE THIS PLAYER? (Y/n)\n> ")
        elif choiceContext == "CONFIRM_SPECIAL_ELECTION":
            verifyChoice = input("ARE YOU SURE YOU " +
                                 "WISH TO ELECT THIS PLAYER? (Y/n)\n> ")
        elif choiceContext == "CONFIRM_EXECUTION":
            verifyChoice = input("ARE YOU SURE YOU " +
                                 "WISH TO EXECUTE THIS PLAYER? (Y/n)\n> ")
        elif choiceContext == "ACCEPT_VETO":
            verifyChoice = input("ARE YOU SURE YOU " +
                                 "WISH TO ACCEPT THIS VETO? (Y/n)\n> ")
        elif choiceContext == "REJECT_VETO":
            verifyChoice = input("ARE YOU SURE YOU " +
                                 "WISH TO REJECT THIS VETO? (Y/n)\n> ")
        # If player inputs anything other than "Y" or "y", cancel the
        # choice by reloading the game, else, return to function invokation
        if verifyChoice.lower() != "y":
            loadGame(gameName)
        else:
            return

    # Clearing the terminal screen and printing the game name with a flair
    # based on the length of the game title
    os.system('cls||clear')
    print("=" * (len(gameName) + 2))
    print(" " + gameName + " ")
    print("=" * (len(gameName) + 2) + "\n")

    # Printing a disclaimer message if the game is hosted
    if isHosting:
        print("YOU ARE HOSTING THIS GAME.\n\n")

    # Printing deck information
    print("LIBERAL POLICIES ENACTED: " + "X " * libEnacted +
          "- " * libNotEnacted)
    print("FASCIST POLICIES ENACTED: " + "X " * fasEnacted +
          "- " * fasNotEnacted)
    print("GOVERNMENT TRACKER: " + "X " * govTracker +
          "- " * govNotTracker)
    print("POLICY DECK: " + str(len(policyDeck)) + " CARDS")
    print("DISCARD PILE: " + str(discardPile) + " CARDS")

    # Print the players participating in the game
    print("\nPLAYERS:")
    # Initialize message to be printed and add upon it depending on context
    # This message is printed individually for each player
    for seat, player in enumerate(gamePlayers):
        message = str(seat + 1) + " - " + player['playerName']
        # If the game is hosted, also print their alignments
        if isHosting:
            message += " - " + player['playerAlignment']
        # During an investigation phase, investigated players are marked
        if isPowerActive == "INVESTIGATE":
            if seat == currentPresident:
                message += " (PRESIDENT)"
            if player['isInvestigated']:
                message += " (INVESTIGATED)"
        else:
            # Executed players are always marked
            if player['isExecuted']:
                message += " (EXECUTED)"
            elif seat == currentPresident:
                # If roles are being distributed, mark the player
                # currently receiving them, else, mark them as president
                if currentContext == "ROLE_DISTRIBUTION":
                    message += " (CURRENT PLAYER)"
                else:
                    message += " (PRESIDENT)"
            elif seat in termLocked:
                # Mark if a player is term-locked
                message += " (TERM-LOCKED)"
            elif seat == currentChancellor:
                message += " (CHANCELLOR)"
        # Print the message
        print(message)

    # Change the message about Hitler's knowledge of the fascists
    # depending on player count
    if len(gamePlayers) <= 6:
        print("\nHitler knows the only fascist on the table.")
    else:
        print("\nHitler doesn't know any of " +
              "the fascists on the table.")
    # Warn about Hitler if at least three Fascist policies have been
    # enacted. Don't warn if six are enacted.
    if fasEnacted >= 3 and fasEnacted <= 5:
        print("From now on, if Hitler enters office as Chancellor, " +
              "THE FASCISTS WIN.\n----\n")
    else:
        print("----\n")

    #############################
    # ROLE DISTRIBUTION CONTEXT #
    #############################

    if currentContext == "ROLE_DISTRIBUTION":
        # Initialize lists for fascist seats and fascist names
        fascistList = []
        fascistNames = []
        # Loop through the game players, taking note of their seat number
        for seat, player in enumerate(gamePlayers):
            playerName = player['playerName']
            playerAlignment = player['playerAlignment']
            if playerAlignment == "Hitler":
                # Assign Hitler related variables
                hitlerPlayer = seat
                hitlerName = playerName
            elif playerAlignment == "Fascist":
                # Assign fascist players into the fascist lists
                fascistList.append(seat)
                fascistNames.append(playerName)
        # Current player's name is saved for easy reference
        # Current player in this context relies on currentPresident variable
        currentPlayer = gamePlayers[currentPresident]['playerName']

        # Print available choices
        print(currentPlayer + " must now receive player alignment " +
              "details. \n\n")
        print("Please select a choice:")
        print("1) Receive player alignment details\n" +
              "2) Exit to main menu\n" +
              "3) Exit program")

        # Make sure input is bound to constraints
        while True:
            mainChoice = input("> ")
            inputCheck, mainChoice = checkIntInput(mainChoice, 1, 3, "ints")
            if inputCheck != "OK":
                print("\n" + inputCheck)
                continue
            else:
                if mainChoice == 2:
                    os.system('cls||clear')
                break

        if mainChoice == 3:
            # Exit program
            exit()
        elif mainChoice == 2:
            # Exit to main menu
            openingFrontend()
        elif mainChoice == 1:
            # Ensure player is sure of their choice to reveal alignment
            verifyChoice("GENERIC")
            if currentPresident == hitlerPlayer:
                # If current player is Hitler, prepare a message saying they
                # are so, and if it's a 6 or 5 player game, tell them of their
                # Fascist buddy
                message = "\nYou are Hitler."
                if len(gamePlayers) > 6:
                    message += " You don't know who your fascist buddies are."
                else:
                    message += " Your fascist buddy is " + fascistNames[0]
            elif currentPresident in fascistList:
                # If current player is Fascist, prepare a message saying they
                # are so, and tell them of their fellow Fascists and Hitler,
                # and take care of formatting the fellow Fascists string
                if len(gamePlayers) > 6:
                    message = "\nYou are Fascist. Your Fascist buddies are "
                    fascistCount = 0
                    for fascist in fascistNames:
                        if fascist == currentPlayer:
                            continue
                        if fascistCount != 0:
                            message += " and "
                        message += fascist
                        fascistCount += 1
                    message += "\nHitler is " + hitlerName
                else:
                    message = "\nYou are Fascist. Hitler is " + hitlerName
            else:
                # If current player is Liberal...tell them that
                message = "\nYou are Liberal."
            # Print the prepared alignment message, as well as a warning
            print(message)
            print("DO NOT PASS YOUR TURN BEFORE DISMISSING THIS SCREEN!")
            if currentPresident + 1 == len(gamePlayers):
                # If currentPresident is the last player on the table,
                # get out of role distribution context
                currentPresident = 0
                currentContext = "NOMINATION"
            else:
                # If currentPresident is not the last player, continue to next
                currentPresident += 1
            saveGame()
            input("Press Return to continue.\n")
        loadGame(gameName)

    ######################
    # NOMINATION CONTEXT #
    ######################

    elif currentContext == "NOMINATION":
        # Retrieve term locked players
        termLocked = currentRound['termLocked']

        # Print available choices
        print(currentPresidentName + " must now nominate a chancellor.\n\n")
        print("Please select a choice:")
        print("1) Nominate a chancellor\n" +
              "2) Exit to main menu\n" +
              "3) Exit program")

        # Make sure input is bound to constraints
        while True:
            mainChoice = input("> ")
            inputCheck, mainChoice = checkIntInput(mainChoice, 1, 3, "ints")
            if inputCheck != "OK":
                print("\n" + inputCheck)
                continue
            else:
                if mainChoice == 2:
                    os.system('cls||clear')
                break

        if mainChoice == 3:
            # Exit program
            exit()
        elif mainChoice == 2:
            # Exit to main menu
            openingFrontend()
        elif mainChoice == 1:
            while True:
                # The player must select a chancellor, failures result in
                # continues, success with a break
                print("Select a number corresponding to your chancellor" +
                      " nomination.")
                chancellorChoice = input("> ")
                # Making sure the input is bound to constraints
                inputCheck, chancellorChoice = checkIntInput(
                    chancellorChoice, 1, len(gamePlayers), "ints")
                if inputCheck != "OK":
                    continue
                else:
                    # Assigning currentChancellor as the player's choice,
                    # minus one to reflect what that dicts begin with zero
                    currentChancellor = chancellorChoice - 1
                    if currentChancellor == currentPresident:
                        # Selected chancellor cannot be current president
                        print(
                            "\nYou cannot choose yourself " +
                            "as chancellor. Try again.")
                        continue
                    if currentChancellor in termLocked:
                        # Selected chancellor cannot be termlocked
                        print("\nThe selected chancellor " +
                              "is term locked. Try again.")
                        continue
                    if gamePlayers[currentChancellor]['isExecuted']:
                        # Selected chancellor cannot be executed
                        print("\nThis player was executed. Try again.")
                        continue
                    # Changing context and assigning chancellor if success
                    currentRound['currentChancellor'] = currentChancellor
                    currentContext = "VOTING"
                    # Preselecting the president for the voting context
                    selectedPlayer = currentPresident
                    break
            saveGame()
            loadGame(gameName)

    ##################
    # VOTING CONTEXT #
    ##################

    elif currentContext == "VOTING":
        # Printing all relevant information for the voting context
        print("President " + currentPresidentName + " has nominated " +
              currentChancellorName + " as Chancellor.\n" +
              "All players must now vote.\n\n")
        print("You are voting as " + selectedPlayerName + ".")
        # Printing votes, if the game is pass and play, only show who voted
        print("VOTES:\n")
        displayVotes(False)

        # Printing available choices
        print("\n\nPlease select a choice:")
        print("1) Vote ja!\n2) Vote nein\n3) Abstain vote\n4) " +
              "Select another player\n5) Select next player" +
              "\n6) Exit to main menu\n7) Exit program")

        # Make sure input is bound to constraints
        while True:
            mainChoice = input("> ")
            inputCheck, mainChoice = checkIntInput(mainChoice, 1, 6, "ints")
            if inputCheck != "OK":
                print("\n" + inputCheck)
            else:
                break
        if mainChoice == 7:
            # Exit program
            exit()
        elif mainChoice == 6:
            # Exit to main menu
            openingFrontend()
        elif mainChoice == 5:
            # Select next player on the table. If player is executed,
            # skip and redo the loop until a non-executed player is found
            while True:
                if selectedPlayer + 1 == len(gamePlayers):
                    selectedPlayer = 0
                else:
                    selectedPlayer += 1
                if gamePlayers[selectedPlayer]['isExecuted']:
                    continue
                else:
                    break
            saveGame()
            loadGame(gameName)
        elif mainChoice == 4:
            print("Select the number corresponding to the " +
                  "player you want to switch to.")
            # Make sure input is bound to constraints, and also
            # that selected player is not executed
            while True:
                mainChoice = input("> ")
                inputCheck, mainChoice = checkIntInput(
                    mainChoice, 1, len(gamePlayers), "ints")
                if inputCheck != "OK":
                    print("\n" + inputCheck)
                else:
                    if gamePlayers[mainChoice - 1]['isExecuted']:
                        print("This player was executed. Try again.")
                    else:
                        break
            # Selected player is saved as player's choice minus one
            selectedPlayer = mainChoice - 1
            saveGame()
            loadGame(gameName)
        elif mainChoice == 3:
            # Change current player's vote to null
            gamePlayers[selectedPlayer]['vote'] = None
            saveGame()
        elif mainChoice == 2:
            # Change current player's vote to no/nein
            gamePlayers[selectedPlayer]['vote'] = "n"
            saveGame()
        elif mainChoice == 1:
            # Change current player's vote to yes/ja!
            gamePlayers[selectedPlayer]['vote'] = "y"
            saveGame()
        # At the end of each vote, initialize variables to be incremented,
        # noneVotes is a bool
        yesVotes = 0
        noVotes = 0
        noneVotes = 0
        for player in gamePlayers:
            # Loop through every player to count votes
            if player['isExecuted']:
                continue
            if player['vote'] is None:
                noneVotes = 1
                break
            elif player['vote'] == "y":
                yesVotes += 1
            elif player['vote'] == "n":
                noVotes += 1
        if not noneVotes:
            # If there is a missing vote, don't continue with this
            if yesVotes > noVotes:
                # If yes votes are more than no votes, check if Chancellor
                # is Hitler after 3F is enacted. If so, the Fascists win,
                # if not, the president enaction context proceeds
                if fasEnacted >= 3:
                    if gamePlayers[
                            currentChancellor]['playerAlignment'] == "Hitler":
                        currentContext = "HITLER_VICTORY"
                    else:
                        currentContext = "PRESIDENT_ENACTION"
                else:
                    currentContext = "PRESIDENT_ENACTION"
                saveGame()
            else:
                # If no votes are more than or equal to yes votes,
                # The government is FAILED and context becomes NOMINATION
                currentContext = "NOMINATION"
                # Record history of failed government and no card enacted
                enactedCard = None
                recordHistory("FAILED_GOVERNMENT")
                os.system('cls||clear')
                print("President " + currentPresidentName + "'s and " +
                      currentChancellorName + "'s government has been " +
                      "rejected!")
                # Display votes in full, along with which votes are which
                print("The votes are:")
                displayVotes(True)
                # Reset votes after having displayed them
                for player in gamePlayers:
                    player['vote'] = None
                # Retrieve the next president in line, reset special election
                currentPresident = retrieveNextPresident(
                    isSpecialElection, currentPresident)
                isSpecialElection = 0
                # Reset current chancellor
                currentChancellor = None
                # Advance government tracker
                govTracker += 1
                if govTracker == 3:
                    # If the government tracker advanced by three steps
                    # then enact top deck policy
                    print("Three governments have failed in a row!")
                    print("Enacting top of the policy deck.")
                    libEnacted, fasEnacted = enactTopDeck()
                    if fasEnacted == 6:
                        currentContext = "FASCIST_VICTORY"
                    elif libEnacted == 5:
                        currentContext = "LIBERAL_VICTORY"
                    # Reset government tracker
                    govTracker = 0
                    # Check if deck needs reshuffling
                    policyDeck, discardPile, isNewDeck = shuffleDeck(
                        policyDeck, discardPile)
                    # Record history for new deck
                    if isNewDeck:
                        recordHistory("NEW_DECK")
                    # Clear termlocked players
                    termLocked.clear()
                # Increment round number
                currentRoundNumber += 1
                saveGame()
                input("Press Return to continue.\n")
        else:
            if not isHosting:
                # If pass and play, show prompt that the vote went through
                saveGame()
                input("You have voted! Press Return to continue.")
        loadGame(gameName)

    ##############################
    # PRESIDENT ENACTION CONTEXT #
    ##############################

    elif currentContext == "PRESIDENT_ENACTION":
        # Printing all relevant info for the president enaction context
        print("President " + currentPresidentName + " and Chancellor " +
              currentChancellorName + " are now in office.\n")
        # Display votes in full detail
        print("VOTES:\n")
        displayVotes(True)

        # Print available choices
        print("\n\nPlease select a choice:")
        print("1) Enact policies as President\n" +
              "2) Exit to main menu\n3) Exit program")

        # Make sure input is bound to constraints
        while True:
            mainChoice = input("> ")
            inputCheck, mainChoice = checkIntInput(mainChoice, 1, 2, "ints")
            if inputCheck != "OK":
                print("\n" + inputCheck)
            else:
                break

        if mainChoice == 3:
            # Exit program
            exit()
        elif mainChoice == 2:
            # Exit to main menu
            openingFrontend()
        elif mainChoice == 1:
            # Initialize policies drawn variable
            currentPolicy = 0
            while currentPolicy < 3:
                # Take the top of the deck card from policy deck three times,
                # and remove it from the policy deck
                currentPolicy += 1
                policiesInHand.append(policyDeck.pop(0))
            # Copy drawn policies to new list for history recording purposes
            policiesDrawn = list(policiesInHand)
            print("You have drawn the following policies:\n")
            for num, currentPolicy in enumerate(policiesInHand):
                # Print drawn policies
                print(str(num + 1) + ") " + currentPolicy)

            print("Please select the policy you wish to DISCARD.")
            while True:
                # Make sure input is bound to constraints
                discardPolicy = input("> ")
                inputCheck, discardPolicy = checkIntInput(
                    discardPolicy, 1, 3, "ints")
                if inputCheck != "OK":
                    print("\n" + inputCheck)
                else:
                    # Remove selected policy from hand,
                    # add it to discard pile
                    policiesInHand.pop(discardPolicy - 1)
                    currentContext = "CHANCELLOR_ENACTION"
                    discardPile += 1
                    saveGame()
                    loadGame(gameName)

    ###############################
    # CHANCELLOR ENACTION CONTEXT #
    ###############################

    elif currentContext == "CHANCELLOR_ENACTION":
        # Print relevant info for chancellor enaction
        print("President " + currentPresidentName + " and Chancellor " +
              currentChancellorName + " are now in office.\n")
        # Print votes in full details
        print("VOTES:\n")
        displayVotes(True)

        # Print available choices
        print("\n\nPlease select a choice:")
        print("1) Enact policies as Chancellor\n" +
              "2) Exit to main menu\n3) Exit program")

        # Check if input is bound to constraints
        while True:
            mainChoice = input("> ")
            inputCheck, mainChoice = checkIntInput(
                mainChoice, 1, 3, "ints")
            if inputCheck != "OK":
                print("\n" + inputCheck)
            else:
                break

        if mainChoice == 3:
            # Exit program
            exit()
        elif mainChoice == 2:
            # Exit to main menu
            openingFrontend()
        elif mainChoice == 1:
            # Printing received policies
            print("You have received the following policies:\n")
            for num, currentPolicy in enumerate(policiesInHand):
                print(str(num + 1) + ") " + currentPolicy)

            # Printing available actions, including vetoes if allowed
            print("Please select the policy you wish to ENACT.")
            if isPowerActive == "VETO" and isVetoRequested == 0:
                print("If you wish to veto this agenda, input \"3\"")

            # Check if input is bound to constraints
            # Constraints are different if vetoes are allowed
            while True:
                enactPolicy = input("> ")
                if isPowerActive == "VETO" and isVetoRequested == 0:
                    # There has to be no prior vetoes requested for this
                    # constraint set to be taken
                    inputCheck, enactPolicy = checkIntInput(
                        enactPolicy, 1, 3, "ints")
                else:
                    inputCheck, enactPolicy = checkIntInput(
                        enactPolicy, 1, 2, "ints")
                if inputCheck != "OK":
                    print("\n" + inputCheck)
                else:
                    if enactPolicy == 3:
                        # If a veto is requested, make isVetoRequested be
                        # true, this bool makes sure no further vetoes
                        # are requested, also context becomes PRESIDENT_VETO
                        currentContext = "PRESIDENT_VETO"
                        isVetoRequested = 1
                        print(currentChancellorName +
                              " wishes to veto the current agenda.")
                        break
                    # The selected card is enacted, the other one discarded
                    enactedCard = policiesInHand.pop(enactPolicy - 1)
                    discardPile += 1
                    if enactedCard == "L":
                        # The enacted card is Liberal
                        libEnacted += 1
                        # NOMINATION will be next context most of the times
                        # on 5th Liberal policy, context is LIBERAL_VICTORY
                        if libEnacted != 5:
                            currentContext = "NOMINATION"
                        else:
                            currentContext = "LIBERAL_VICTORY"
                        # Record history for liberal government
                        recordHistory("LIBERAL_GOVERNMENT")
                        # Clear current termlocked players
                        termLocked.clear()
                        # Add chancellor and possible president as termlocked
                        if alivePlayers != 5:
                            termLocked.append(currentPresident)
                        termLocked.append(currentChancellor)
                        # Clear current chancellor
                        currentChancellor = None
                        # Retrieve next president, clear special election
                        currentPresident = retrieveNextPresident(
                            isSpecialElection, currentPresident)
                        isSpecialElection = 0
                        # Clear votes and policies drawn
                        for iter in gamePlayers:
                            iter['vote'] = None
                        policiesDrawn = []
                        # Advance to next round
                        currentRoundNumber += 1
                        # Announce the policy enaction
                        os.system('cls||clear')
                        print("A Liberal policy has been enacted.")
                        print("There are now " + str(libEnacted) +
                              " enacted Liberal policies.")
                    else:
                        # The enacted card is Fascist
                        fasEnacted += 1
                        # Retrieve the power to be used in this scenario
                        isPowerActive = retrieveFascistPower()
                        if isPowerActive == "NULL" or fasEnacted == 6:
                            # If there are no powers, enact Fascist as normal
                            # If 6th Fascist enacted, context is
                            # FASCIST_VICTORY, otherwise NOMINATION
                            if fasEnacted != 6:
                                currentContext = "NOMINATION"
                            else:
                                currentContext = "FASCIST_VICTORY"
                            # Record history for Fascist government
                            recordHistory("FASCIST_GOVERNMENT")
                            # Clear current termlocked players
                            termLocked.clear()
                            # Retrieve next president, clear special election
                            if alivePlayers != 5:
                                termLocked.append(currentPresident)
                            termLocked.append(currentChancellor)
                            # Clear current chancellor and special election
                            currentChancellor = None
                            currentPresident = retrieveNextPresident(
                                isSpecialElection, currentPresident)
                            isSpecialElection = 0
                            # Clear vots and policies drawn
                            for iter in gamePlayers:
                                iter['vote'] = None
                            policiesDrawn = []
                            # Advance to next round
                            currentRoundNumber += 1
                        else:
                            currentContext = "FASCIST_POWER"
                        # Announce the policy enaction
                        os.system('cls||clear')
                        print("A Fascist policy has been enacted.")
                        print("There are now " + str(fasEnacted) +
                              " enacted Fascist policies.")
                        # Announce the power to be used, if any
                        if isPowerActive == "POLICY_PEAK":
                            print(
                                currentPresidentName + " must now peak at " +
                                "the top three cards of the policy deck.")
                        elif isPowerActive == "INVESTIGATE":
                            print(currentPresidentName +
                                  " must now investigate a player.")
                        elif isPowerActive == "SPECIAL_ELECTION":
                            # Save the direct next president in line
                            # this will be referred to after the
                            # special government
                            if currentPresident + 1 == len(gamePlayers):
                                nextPresident = 0
                            else:
                                nextPresident = currentPresident + 1
                            print(currentPresidentName +
                                  " must now select a player for Special " +
                                  "Election.")
                        elif isPowerActive == "EXECUTION":
                            print(currentPresidentName +
                                  " must now execute a player.")

                    # Clear policies in hand
                    policiesInHand = []
                    # Generate new deck and record history
                    policyDeck, discardPile, isNewDeck = shuffleDeck(
                        policyDeck, discardPile)
                    if isNewDeck:
                        recordHistory("NEW_DECK")
                    # Reset government tracker and requested veto
                    govTracker = 0
                    isVetoRequested = 0
                    break
            saveGame()
            input("Press Return to continue.\n")
            loadGame(gameName)

    ##########################
    # PRESIDENT VETO CONTEXT #
    ##########################

    elif currentContext == "PRESIDENT_VETO":
        # Print relevant info
        print("Chancellor " + currentChancellorName + " wishes to veto the" +
              " current agenda under President " + currentPresidentName)
        # Print votes in full
        print("VOTES:\n")
        displayVotes(True)

        # Print available choices
        print("\n\nPlease select a choice:")
        print("1) Accept veto\n2) Reject veto\n" +
              "3) Exit to main menu\n4) Exit program")

        # Make sure input is bound to constraints
        while True:
            mainChoice = input("> ")
            inputCheck, mainChoice = checkIntInput(mainChoice, 1, 4, "ints")
            if inputCheck != "OK":
                print("\n" + inputCheck)
            else:
                break

        if mainChoice == 4:
            # Exit program
            exit()
        elif mainChoice == 3:
            # Exit to main menu
            openingFrontend()
        elif mainChoice == 2:
            # Verify that president wants to reject veto
            verifyChoice("REJECT_VETO")
            # Set context to CHANCELLOR_ENACTION and print relevent info
            currentContext = "CHANCELLOR_ENACTION"
            print("President " + currentPresidentName + " has chosen to" +
                  " reject Chancellor " + currentChancellorName + "'s" +
                  " proposed veto.")
        elif mainChoice == 1:
            # Verify that president wants to accept veto
            verifyChoice("ACCEPT_VETO")
            # Change context to nomination, discard two cards at once,
            # do enact any policies
            currentContext = "NOMINATION"
            enactedCard = None
            discardPile += 2
            # Record history of vetoed government
            recordHistory("VETOED_GOVERNMENT")
            # Clear drawn policies
            policiesDrawn = []
            policiesInHand = []
            # Clear votes
            for player in gamePlayers:
                player['vote'] = None
            # Retrieve next president, not sure why clear special election
            currentPresident = retrieveNextPresident(
                isSpecialElection, currentPresident)
            isSpecialElection = 0
            # Announce successful veto
            print("The government has been vetoed!")
            # Clear current chancellor, advance round, clear isVetoRequested
            currentChancellor = None
            currentRoundNumber += 1
            isVetoRequested = 0
            # Advance government tracker
            govTracker += 1
            if govTracker == 3:
                # Enact top deck and reset special election
                print("Three governments have failed in a row!")
                print("Enacting top of the policy deck.")
                libEnacted, fasEnacted = enactTopDeck()
                if fasEnacted == 6:
                    currentContext = "FASCIST_VICTORY"
                elif libEnacted == 5:
                    currentContext = "LIBERAL_VICTORY"
                govTracker = 0
                if isSpecialElection:
                    isSpecialElection = 0
                # Clear termlocked players
                termLocked.clear()
            # Generate new deck if needed and record history for it
            policyDeck, discardPile, isNewDeck = shuffleDeck(
                policyDeck, discardPile)
            if isNewDeck:
                recordHistory("NEW_DECK")
        saveGame()
        input("Press Return to continue.")
        loadGame(gameName)

    #########################
    # FASCIST POWER CONTEXT #
    #########################

    elif currentContext == "FASCIST_POWER":
        # Election check is used so not to override special election
        electionCheck = 0

        # ----------------- #
        # POLICY PEAK power #
        # ----------------- #

        if isPowerActive == "POLICY_PEAK":
            # Announce policy peak and print available choices
            print("President " + currentPresidentName +
                  " must perform a policy peak.")
            print("Please select a choice:")
            print("1) Perform policy peak\n" +
                  "2) Exit to main menu\n" +
                  "3) Exit program")

            # Make sure input is bound to constraints
            while True:
                mainChoice = input("> ")
                inputCheck, mainChoice = checkIntInput(
                    mainChoice, 1, 3, "ints")
                if inputCheck != "OK":
                    print("\n" + inputCheck)
                    continue
                else:
                    if mainChoice == 2:
                        os.system('cls||clear')
                    break

            if mainChoice == 3:
                # Exit program
                exit()
            elif mainChoice == 2:
                # Exit to main menu
                openingFrontend()
            elif mainChoice == 1:
                # Make sure the current player is only able to peak
                verifyChoice("GENERIC")
                print("\nPresident " + currentPresidentName +
                      " performs the policy peak.")
                print("The top three policy deck cards are:")
                # Print the top three cards in the policy
                # deck without removing them
                for policy in policyDeck[0:3]:
                    print(policy)

        # ----------------- #
        # INVESTIGATE power #
        # ----------------- #

        elif isPowerActive == "INVESTIGATE":
            # Announce investigation and print available choices
            print("President " + currentPresidentName +
                  " must investigate a player.")
            print("Please select a choice:")
            print("1) Perform investigation\n" +
                  "2) Exit to main menu\n" +
                  "3) Exit program")

            # Make sure input is bound to constraints
            while True:
                mainChoice = input("> ")
                inputCheck, mainChoice = checkIntInput(
                    mainChoice, 1, 3, "ints")
                if inputCheck != "OK":
                    print("\n" + inputCheck)
                    continue
                else:
                    if mainChoice == 2:
                        os.system('cls||clear')
                    break

            if mainChoice == 3:
                # Exit program
                exit()
            elif mainChoice == 2:
                # Exit to main menu
                openingFrontend()
            elif mainChoice == 1:
                # Make sure only current player can see investigation results
                verifyChoice("GENERIC")
                while True:
                    # Request a choice from the president and make
                    # sure it is bound to constraints
                    print("Select a number corresponding to your " +
                          "investigation target.")
                    powerTarget = input("> ")
                    inputCheck, powerTarget = checkIntInput(
                        powerTarget, 1, len(gamePlayers), "ints")
                    if inputCheck != "OK":
                        print("\n" + inputCheck)
                    else:
                        # Ensure president is sure of their ivnestigation
                        # and assign the choice as powerTarget
                        verifyChoice("CONFIRM_INVESTIGATION")
                        powerTarget -= 1
                        targetPlayer = gamePlayers[powerTarget]
                        if powerTarget == currentPresident:
                            # President cannot investigate himself
                            print("\nYou cannot investigate yourself. " +
                                  "Try again.")
                            continue
                        elif targetPlayer['isInvestigated']:
                            # An investigated person cannot be investigated
                            print(
                                "\nThis player has been investigated " +
                                "before. Try again.")
                            continue
                        else:
                            # If investigation is legal, return alignment
                            print("President " + currentPresidentName +
                                  " performs an investigation of " +
                                  targetPlayer['playerName'] + ".")
                            # A Liberal returns Liberal, both Fascist
                            # and Hitler return Fascist
                            if targetPlayer['playerAlignment'] == "Liberal":
                                print(targetPlayer
                                      ['playerName'] + " is Liberal.")
                            else:
                                print(targetPlayer
                                      ['playerName'] + " is Fascist.")
                            # Make the investigated player's isInvestigated
                            # boolean true
                            gamePlayers[powerTarget]['isInvestigated'] = True
                            break

        # ---------------------- #
        # SPECIAL ELECTION power #
        # ---------------------- #

        elif isPowerActive == "SPECIAL_ELECTION":
            # Announce special election and print avalable choices
            print("President " + currentPresidentName +
                  " must choose a player for the special election.")
            print("Please select a choice:")
            print("1) Select special election president\n" +
                  "2) Exit to main menu\n" +
                  "3) Exit program")

            # Make sure input is bound to constraints
            while True:
                mainChoice = input("> ")
                inputCheck, mainChoice = checkIntInput(
                    mainChoice, 1, 3, "ints")
                if inputCheck != "OK":
                    print("\n" + inputCheck)
                    continue
                else:
                    if mainChoice == 2:
                        os.system('cls||clear')
                    break

            if mainChoice == 3:
                # Exit program
                exit()
            elif mainChoice == 2:
                # Exit to main menu
                openingFrontend()
            elif mainChoice == 1:
                while True:
                    # Player should select special election president\n
                    # And also make sure the input is bound to constraints
                    print("Select a number corresponding to your special" +
                          " election target.")
                    powerTarget = input("> ")
                    inputCheck, powerTarget = checkIntInput(
                        powerTarget, 1, len(gamePlayers), "ints")
                    if inputCheck != "OK":
                        print("\n" + inputCheck)
                    else:
                        # Make sure player is sure about their choice
                        verifyChoice("CONFIRM_SPECIAL_ELECTION")
                        if powerTarget - 1 == currentPresident:
                            # You cannot elect yourself
                            print(
                                "\nYou cannot elect yourself for " +
                                "the Special Election. Try again.")
                            continue
                        else:
                            # Set election target, enable special
                            # election, and electionCheck
                            powerTarget -= 1
                            isSpecialElection = 1
                            electionCheck = 1
                            break

        # --------------- #
        # EXECUTION power #
        # --------------- #

        elif isPowerActive == "EXECUTION":
            # Announce execution and print available choices
            print("President " + currentPresidentName +
                  " must choose a player to execute.")
            print("Please select a choice:")
            print("1) Execute a player\n" +
                  "2) Exit to main menu\n" +
                  "3) Exit program")

            # Ensure input is bound to constraints
            while True:
                mainChoice = input("> ")
                inputCheck, mainChoice = checkIntInput(
                    mainChoice, 1, 3, "ints")
                if inputCheck != "OK":
                    print("\n" + inputCheck)
                    continue
                else:
                    if mainChoice == 2:
                        os.system('cls||clear')
                    break

            if mainChoice == 3:
                # Exit program
                exit()
            elif mainChoice == 2:
                # Exit to main menu
                openingFrontend()
            elif mainChoice == 1:
                while True:
                    # Ask the player for their execution choice
                    # and make sure input is bound to constraints
                    print("Select a number corresponding to your" +
                          " execution target.")
                    powerTarget = input("> ")
                    inputCheck, powerTarget = checkIntInput(
                        powerTarget, 1, len(gamePlayers), "ints")
                    if inputCheck != "OK":
                        print("\n" + inputCheck)
                    else:
                        # Make sure the player is sure of their
                        # execution choice
                        verifyChoice("CONFIRM_EXECUTION")
                        if powerTarget - 1 == currentPresident:
                            # Don't be silly
                            print(
                                "\nWhy would you execute yourself? Try again.")
                            continue
                        else:
                            exTarget = gamePlayers[powerTarget - 1]
                            if exTarget['isExecuted']:
                                # Can't execute someone who was executed
                                print("This player is already " +
                                      "executed. Try again.")
                                continue
                            else:
                                # Decrease number of alive players and
                                # mark executed player as so
                                exTarget['isExecuted'] = 1
                                executedName = exTarget['playerName']
                                alivePlayers -= 1
                                print("President " + currentPresidentName +
                                      " has executed " + executedName + ".")
                                break

        # All these apply regardless of the power used
        # Enact an F policy
        enactedCard = "F"
        if isPowerActive == "EXECUTION":
            # If it's an execution and the target was Hitler, end the game
            if exTarget['playerAlignment'] == "Hitler":
                currentContext = "HITLER_EXECUTION_VICTORY"
            else:
                currentContext = "NOMINATION"
        else:
            currentContext = "NOMINATION"
        # Record history for a Fascist government
        recordHistory("FASCIST_GOVERNMENT")
        # Clear active powers if less than five policies enacted
        # but if five policies enacted, keep it at VETO
        if fasEnacted != 5:
            isPowerActive = "NULL"
        else:
            isPowerActive = "VETO"
        # Clear termlocked players
        termLocked.clear()
        # Add chancellor and possibly president to termlocked players
        if alivePlayers != 5:
            termLocked.append(currentPresident)
        termLocked.append(currentChancellor)
        # Clear current chancellor
        currentChancellor = None
        if electionCheck == 0:
            # If electionCheck is false, clear special election
            # and retrieve the next president
            currentPresident = retrieveNextPresident(
                isSpecialElection, currentPresident)
            isSpecialElection = 0
        else:
            # Else, assign the powerTarget as special election president
            currentPresident = powerTarget
        # Clear all votes
        for iter in gamePlayers:
            iter['vote'] = None
        # Clear policies and advance round number by one
        policiesDrawn = []
        currentRoundNumber += 1
        saveGame()
        input("Press Return to continue.\n")
        loadGame(gameName)

    #########################################
    # VICTORY BY ELECTION OF HITLER CONTEXT #
    #########################################

    elif currentContext == "HITLER_VICTORY":
        # The Fascists have won by electing Hitler after 3 Fascists enacted
        # Announce Hitler's identity and his fascists' identities
        print("Chancellor " + currentChancellorName +
              " is Hitler! The fascists have won!")
        if len(gamePlayers) > 6:
            fascistMessage = "Hitler's Fascist buddies are "
        else:
            fascistMessage = "Hitler's Fascist buddy is "
        fascistCount = 0
        for player in gamePlayers:
            if player['playerAlignment'] == "Fascist":
                if fascistCount != 0:
                    fascistMessage += ", "
                fascistMessage += player['playerName']
                fascistCount += 1
        # Print fascists' identity and print available choices
        print(fascistMessage + "\n\n")
        print("Please select a choice:")
        print("1) Exit to main menu\n" +
              "2) Exit program")

        # Make sure input is bound to constraints
        while True:
            mainChoice = input("> ")
            inputCheck, mainChoice = checkIntInput(mainChoice, 1, 3, "ints")
            if inputCheck != "OK":
                print("\n" + inputCheck)
                continue
            else:
                if mainChoice == 2:
                    os.system('cls||clear')
                break

        if mainChoice == 2:
            # Exit program
            exit()
        elif mainChoice == 1:
            # Exit to main menu
            openingFrontend()

    ##########################################
    # VICTORY BY EXECUTION OF HITLER CONTEXT #
    ##########################################

    elif currentContext == "HITLER_EXECUTION_VICTORY":
        # The Liberals have won by execution of Hitler
        for seat, player in enumerate(gamePlayers):
            if player['playerAlignment'] == "Hitler":
                hitlerPlayer = seat
                break
        # Obtain Hitler's player name
        hitlerName = gamePlayers[hitlerPlayer]['playerName']
        # Announce victory and Hitler's identity
        print("Hitler has been executed! The Liberals have won!")
        print("Hitler was " + hitlerName)
        # Announce Hitler's Fascist buddies
        if len(gamePlayers) > 6:
            fascistMessage = "Hitler's Fascist buddies are "
        else:
            fascistMessage = "Hitler's Fascist buddy is "
        # fascistCount is used to count the number of Fascists counted through
        fascistCount = 0
        for player in gamePlayers:
            if player['playerAlignment'] == "Fascist":
                if fascistCount != 0:
                    fascistMessage += ", "
                fascistMessage += player['playerName']
                fascistCount += 1
        # Print fascists' identity and print available choices
        print(fascistMessage + "\n\n")
        print("Please select a choice:")
        print("1) Exit to main menu\n" +
              "2) Exit program")

        # Make sure input i sbound to constraints
        while True:
            mainChoice = input("> ")
            inputCheck, mainChoice = checkIntInput(mainChoice, 1, 3, "ints")
            if inputCheck != "OK":
                print("\n" + inputCheck)
                continue
            else:
                if mainChoice == 2:
                    os.system('cls||clear')
                break

        if mainChoice == 2:
            # Exit program
            exit()
        elif mainChoice == 1:
            # Exit to main menu
            openingFrontend()

    ###########################
    # LIBERAL VICTORY CONTEXT #
    ###########################

    elif currentContext == "LIBERAL_VICTORY":
        # The Liberals have won by enacting 5 Liberal policies
        for seat, player in enumerate(gamePlayers):
            # Obtain Hitler's seat number
            if player['playerAlignment'] == "Hitler":
                hitlerPlayer = seat
                break
        # Obtain Hitler's player name
        hitlerName = gamePlayers[hitlerPlayer]['playerName']
        # Announce victory and Hitler's identity
        print("Five Liberal policies have been enacted! " +
              "The Liberals have won!")
        print("Hitler was " + hitlerName)
        # Announce Hitler's fascist buddies
        if len(gamePlayers) > 6:
            fascistMessage = "Hitler's Fascist buddies are "
        else:
            fascistMessage = "Hitler's Fascist buddy is "
        # fascistCount is used to count the number of Fascists counted through
        fascistCount = 0
        for player in gamePlayers:
            if player['playerAlignment'] == "Fascist":
                if fascistCount != 0:
                    fascistMessage += ", "
                fascistMessage += player['playerName']
                fascistCount += 1
        # Print fascists' identities and print available choices
        print(fascistMessage + "\n\n")
        print("Please select a choice:")
        print("1) Exit to main menu\n" +
              "2) Exit program")

        # Make sure input is bound to constraints
        while True:
            mainChoice = input("> ")
            inputCheck, mainChoice = checkIntInput(mainChoice, 1, 3, "ints")
            if inputCheck != "OK":
                print("\n" + inputCheck)
                continue
            else:
                if mainChoice == 2:
                    os.system('cls||clear')
                break

        if mainChoice == 2:
            # Exit program
            exit()
        elif mainChoice == 1:
            # Exit to main menu
            openingFrontend()

    ###########################
    # FASCIST VICTORY CONTEXT #
    ###########################

    elif currentContext == "FASCIST_VICTORY":
        # The Fascists have won by enacting 6 Fascist policies
        for seat, player in enumerate(gamePlayers):
            # Obtain Hitler's seat number
            if player['playerAlignment'] == "Hitler":
                hitlerPlayer = seat
                break
        # Obtain Hitler's player name
        hitlerName = gamePlayers[hitlerPlayer]['playerName']
        # Announce victory and Hitler's identity
        print("Six Fascist policies have been enacted! " +
              "The Fascists have won!")
        print("Hitler was " + hitlerName)
        # Announce Hitler's Fascist buddies
        if len(gamePlayers) > 6:
            fascistMessage = "Hitler's Fascist buddies are "
        else:
            fascistMessage = "Hitler's Fascist buddy is "
        # fascistCount is used to count the number of Fascists counted through
        fascistCount = 0
        for player in gamePlayers:
            if player['playerAlignment'] == "Fascist":
                if fascistCount != 0:
                    fascistMessage += ", "
                fascistMessage += player['playerName']
                fascistCount += 1
        # Print fascists' identity and print available choices
        print(fascistMessage + "\n\n")
        print("Please select a choice:")
        print("1) Exit to main menu\n" +
              "2) Exit program")

        # Make sure input is bound to constraints
        while True:
            mainChoice = input("> ")
            inputCheck, mainChoice = checkIntInput(mainChoice, 1, 3, "ints")
            if inputCheck != "OK":
                print("\n" + inputCheck)
                continue
            else:
                if mainChoice == 2:
                    os.system('cls||clear')
                break

        if mainChoice == 2:
            # Exit program
            exit()
        elif mainChoice == 1:
            # Exit to main menu
            openingFrontend()


# This function initializes the program with the main menu
openingFrontend()
